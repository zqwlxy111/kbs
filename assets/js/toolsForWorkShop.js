window.yvanForWorkShop = {

    /** @MethodName: getDynamicTime
     * @Description: 根据赋予ID值实现动态刷新时间的方法
     * @Return:
     * @Author: Yvan
     * @Date: 2019/8/27/0027  13:55
     *
     */
    getDynamicTime: function () {
        //分别获取年、月、日、时、分、秒
        var myDate = new Date();
        var year = myDate.getFullYear();
        var month = myDate.getMonth() + 1;
        var date = myDate.getDate();
        var hours = myDate.getHours();
        var minutes = myDate.getMinutes();
        var seconds = myDate.getSeconds();
        //月份的显示为两位数字如09月
        if (month < 10) {
            month = "0" + month;
        }
        if (date < 10) {
            date = "0" + date;
        }
        //时间拼接
        var dateTime = year + "年" + month + "月" + date + "日" + hours + "时" + minutes + "分" + seconds + "秒";
        //document.write(dateTime);//打印当前时间
        var divNode = document.getElementById("time");
        divNode.innerHTML = dateTime;
    },

    /** @MethodName: addZero
     * @Description: 补零操作
     * @Return:
     * @Author: Yvan
     * @Date: 2019/8/27/0027  14:31
     *
     */
    addZero: function (num) {
        if (parseInt(num) < 10) {
            num = '0' + num;
        }
        return num;
    },

    /** @MethodName: getMyDate
     * @Description: 把时间字符串转换为标准时间
     * 参数说明： str: 时间字符串 string:y-m-d-h-i-s 选择转换格式 li: true 默认格式 / false: 添加中文格式
     * @Return:
     * @Author: Yvan
     * @Date: 2019/8/27/0027  14:03
     *
     */
    getMyDate: function (str, string, li) {
        str = typeof (str) == "string" ? parseInt(str) : str; // 判断变量是否为String类型
        var oDate = new Date(str),
            oYear = oDate.getFullYear(),        // 获取年份
            oMonth = oDate.getMonth() + 1,      // 获取月份
            oDay = oDate.getDate(),             // 获取天数
            oHour = oDate.getHours(),           // 获取小时
            oMin = oDate.getMinutes(),          // 获取分钟
            oSen = oDate.getSeconds();          // 获取秒数
        li = li == undefined ? true : false;
        switch (string) {
            case 'y' :                          // 年
                if (li) {
                    oTime = oYear;
                } else {
                    oTime = oYear + "年"
                }
                break;
            case 'm' :                          // 月
                if (li) {
                    oTime = yvan.addZero(oMonth);
                } else {
                    oTime = yvan.addZero(oMonth) + "月";
                }
                break;
            case 'd' :                          // 日
                if (li) {
                    oTime = yvan.addZero(oDay);
                } else {
                    oTime = yvan.addZero(oDay) + "日";
                }
                break;
            case 'h' :                          // 小时
                if (li) {
                    oTime = yvan.addZero(oHour);
                } else {
                    oTime = yvan.addZero(oHour) + "时";
                }
                break;
            case 'i' :                          // 分
                if (li) {
                    oTime = yvan.addZero(oMin);
                } else {
                    oTime = yvan.addZero(oMin) + "分";
                }
                break;
            case 's' :                          // 秒
                if (li) {
                    oTime = yvan.addZero(oSen);
                } else {
                    oTime = yvan.addZero(oSen) + "秒";
                }
                break;
            case 'y-m' :                          // 年-月
                if (li) {
                    oTime = oYear + '-' + yvan.addZero(oMonth);
                } else {
                    oTime = oYear + '年' + yvan.addZero(oMonth) + "月";
                }
                break;
            case 'y-m-d' :                          // 年-月-日
                if (li) {
                    oTime = oYear + '-' + yvan.addZero(oMonth) + '-' + yvan.addZero(oDay);
                } else {
                    oTime = oYear + '年' + yvan.addZero(oMonth) + '月' + yvan.addZero(oDay) + "日";
                }
                break;
            case 'y-m-d-h' :                          // 年-月-日 时
                if (li) {
                    oTime = oYear + '-' + yvan.addZero(oMonth) + '-' + yvan.addZero(oDay) + ' ' + yvan.addZero(oHour);
                } else {
                    oTime = oYear + '年' + yvan.addZero(oMonth) + '月' + yvan.addZero(oDay) + "日" + ' ' + yvan.addZero(oHour) + "时";
                }
                break;
            case 'y-m-d-h-i' :                          // 年-月-日 时-分
                if (li) {
                    oTime = oYear + '-' + yvan.addZero(oMonth) + '-' + yvan.addZero(oDay) + ' ' + yvan.addZero(oHour) + ':' + yvan.addZero(oMin);
                } else {
                    oTime = oYear + '年' + yvan.addZero(oMonth) + '月' + yvan.addZero(oDay) + "日" + ' ' + yvan.addZero(oHour) + "时" + yvan.addZero(oMin) + "分";
                }
                break;
            case 'y-m-d-h-i-s' :                          // 年-月-日 时-分-秒
                if (li) {
                    oTime = oYear + '-' + yvan.addZero(oMonth) + '-' + yvan.addZero(oDay) + ' ' + yvan.addZero(oHour) + ':' + yvan.addZero(oMin) + ':' + yvan.addZero(oSen);
                } else {
                    oTime = oYear + '年' + yvan.addZero(oMonth) + '月' + yvan.addZero(oDay) + "日" + ' ' + yvan.addZero(oHour) + "时" + yvan.addZero(oMin) + "分" + yvan.addZero(oSen) + "秒";
                }
                break;
            case 'm-d' :                          // 月-日
                if (li) {
                    oTime = yvan.addZero(oMonth) + '-' + yvan.addZero(oDay);
                } else {
                    oTime = yvan.addZero(oMonth) + '月' + yvan.addZero(oDay) + "日";
                }
                break;
            case 'm-d-h-i' :                          // 月-日 时-分
                if (li) {
                    oTime = yvan.addZero(oMonth) + '-' + yvan.addZero(oDay) + ' ' + yvan.addZero(oHour) + ':' + yvan.addZero(oMin);
                } else {
                    oTime = yvan.addZero(oMonth) + '月' + yvan.addZero(oDay) + "日" + ' ' + yvan.addZero(oHour) + "时" + yvan.addZero(oMin) + "分";
                }
                break;
            case 'm-d-h-i-s' :                          // 月-日 时-分-秒
                if (li) {
                    oTime = yvan.addZero(oMonth) + '-' + yvan.addZero(oDay) + ' ' + yvan.addZero(oHour) + ':' + yvan.addZero(oMin) + ':' + yvan.addZero(oSen);
                } else {
                    oTime = yvan.addZero(oMonth) + '月' + yvan.addZero(oDay) + "日" + ' ' + yvan.addZero(oHour) + "时" + yvan.addZero(oMin) + "分" + yvan.addZero(oSen) + "秒";
                }
                break;
            case 'h-i' :                          // 时-分
                if (li) {
                    oTime = yvan.addZero(oHour) + ':' + yvan.addZero(oMin);
                } else {
                    oTime = yvan.addZero(oHour) + "时" + yvan.addZero(oMin) + "分";
                }
                break;
            case 'i-s' :                          // 分-秒
                if (li) {
                    oTime = yvan.addZero(oMin) + ':' + yvan.addZero(oSen);
                } else {
                    oTime = yvan.addZero(oMin) + "分" + yvan.addZero(oSen) + "秒";
                }
                break;
            case 'h-i-s' :                          // 时-分-秒
                if (li) {
                    oTime = yvan.addZero(oHour) + ':' + yvan.addZero(oMin) + ':' + yvan.addZero(oSen);
                } else {
                    oTime = yvan.addZero(oHour) + "时" + yvan.addZero(oMin) + "分" + yvan.addZero(oSen) + "秒";
                }
                break;
        }
        return oTime;
    },

    /** @MethodName: toPrecent
     * @Description: 转换百分数
     * 参数说明：number：保留几位小数
     * @Return:
     * @Author: Yvan
     * @Date: 2019/8/27/0027  15:08
     *
     */
    toPercent: function (point, number) {
        var str = Number(point * 100).toFixed(number);
        str += "%";
        return str;
    },

    /** @MethodName: passCookie
     * @Description: 检测cookie
     * @Return:
     * @Author: Yvan
     * @Date: 2019/8/27/0027  16:04
     *
     */
    passCookie: function () {
        if (document.cookie == '') {
            alert("用户已退出！请重新登录！！");
            window.parent.location.href = '/kbs/login.html';
        }
    },

    /** @MethodName: passChrome
     * @Description: 检测是否是Chrome浏览器
     * @Return:
     * @Author: Yvan
     * @Date: 2019/8/27/0027  16:06
     *
     */
    passChrome: function () {
        var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
        var isOpera = userAgent.indexOf("Opera") > -1;
        if (!(userAgent.indexOf("Chrome") > -1)) {
            alert("检测到这不是Google Chrome浏览器!!! 请使用Google Chrome浏览器!!!");
            window.location.href = "http://172.20.212.196:8080/site";
        }
    },

    /** @MethodName: getMyCharts
     * @Description: 车间级柱形图(左边)
     * @Return:
     * @Author: Yvan
     * @Date: 2019/8/29/0029  9:49
     *
     */
    getMyChartsL: function (eleId, config) {

        // 渲染活动情况预测
        var myCharts = echarts.init(document.getElementById(eleId), myEchartsTheme);

        // 标签设置
        var labelOption = {
            normal: {
                rotate: 90,
                align: 'center',
                verticalAlign: 'middle',
                position: 'top',
                distance: 14,
                show: true,
                // formatter: '{c}%',
                rich: {
                    name: {
                        textBorderColor: '#fff'
                    }
                },
                textStyle: {
                    //文字颜色
                    color: '#000',
                    //字体风格,'normal','italic','oblique'
                    fontStyle: 'normal',
                    //字体粗细 'normal','bold','bolder','lighter',100 | 200 | 400 | 400...
                    fontWeight: 200,
                    //字体系列
                    // fontFamily: 'sans-serif',
                    //字体大小
                    fontSize: 12
                }
            }
        };
        var option = {
            title: {
                show: true,
                text: config.text,
                // 标题居中
                x: 'center',
                y: 'top',
                textStyle: {
                    //文字颜色
                    color: '#0f396f',
                    //字体风格,'normal','italic','oblique'
                    fontStyle: 'normal',
                    //字体粗细 'normal','bold','bolder','lighter',100 | 200 | 400 | 400...
                    fontWeight: 'normal',
                    //字体系列
                    fontFamily: 'sans-serif',
                    //字体大小
                    fontSize: 17
                }
            },
            legend: {
                x: 'right',
                y: '32px',
                data: config.legend.data,
                textStyle: {
                    //字体风格,'normal','italic','oblique'
                    fontStyle: 'normal',
                    //字体粗细 'normal','bold','bolder','lighter',100 | 200 | 400 | 400...
                    fontWeight: 'normal',
                    //字体系列
                    fontFamily: 'sans-serif',
                    //字体大小
                    fontSize: 12
                }
            },
            tooltip: {
                trigger: 'axis'
            },
            calculable: true,
            xAxis: [
                {
                    type: 'category',
                    show: true,
                    // data: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', '智能1号线']
                    data: config.xData
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    axisLabel: {
                        show: true,
                        interval: 'auto',
                        // formatter: '{value}%'
                    },
                    show: true
                }
            ],
            grid: {
                x: 37,
                y: 72,
                x2: 0,
                y2: 21,
                borderWidth: 1
            },
            series: [
                {
                    name: '生产自然台',
                    type: 'bar',
                    barGap: 0,
                    label: labelOption,
                    data: config.scZiRanTai,
                    smooth: false, // 当为true时，就是光滑的曲线（默认为true）；当为false，就是折线不是曲线的了
                    symbolSize: 7, // 拐点圆的大小
                },
                {
                    name: '生产标台数',
                    type: 'bar',
                    label: labelOption,
                    data: config.scBiaoTaiShu,
                    smooth: false, // 当为true时，就是光滑的曲线（默认为true）；当为false，就是折线不是曲线的了
                    symbolSize: 7, // 拐点圆的大小
                }
            ]
        };
        myCharts.setOption(option);
        return myCharts;
    },

    /** @MethodName: getMyCharts
     * @Description: 车间级折线图(右边)
     * @Return:
     * @Author: Yvan
     * @Date: 2019/8/29/0029  9:49
     *
     */
    getMyChartsR: function (eleId, config) {

        // 渲染活动情况预测
        var myCharts = echarts.init(document.getElementById(eleId), myEchartsTheme);

        // 标签设置
        var labelOption = {
            normal: {
                rotate: 0,
                align: 'left',
                verticalAlign: 'middle',
                position: 'top',
                distance: 14,
                show: true,
                formatter: '{c}%',
                rich: {
                    name: {
                        textBorderColor: '#fff'
                    }
                },
                textStyle: {
                    //文字颜色
                    color: '#000',
                    //字体风格,'normal','italic','oblique'
                    fontStyle: 'normal',
                    //字体粗细 'normal','bold','bolder','lighter',100 | 200 | 300 | 400...
                    fontWeight: 200,
                    //字体系列
                    // fontFamily: 'sans-serif',
                    //字体大小
                    fontSize: 12
                }
            }
        };

        var option = {
            title: {
                text: config.text,
                show: true,
                // 标题居中
                x: 'center',
                y: 'top',
                textStyle: {
                    //文字颜色
                    color: '#0f396f',
                    //字体风格,'normal','italic','oblique'
                    fontStyle: 'normal',
                    //字体粗细 'normal','bold','bolder','lighter',100 | 200 | 300 | 400...
                    fontWeight: 'normal',
                    //字体系列
                    fontFamily: 'sans-serif',
                    //字体大小
                    fontSize: 17
                }
            },
            legend: {
                x: 'right',
                y: '32px',
                data: config.legend.data,
                textStyle: {
                    //字体风格,'normal','italic','oblique'
                    fontStyle: 'normal',
                    //字体粗细 'normal','bold','bolder','lighter',100 | 200 | 400 | 400...
                    fontWeight: 'normal',
                    //字体系列
                    fontFamily: 'sans-serif',
                    //字体大小
                    fontSize: 12
                }
            },
            tooltip: {
                trigger: 'axis',
                // formatter: '{b0}<br />{a0}: {c0}%<br />{a1}: {c1}%<br />{a2}: {c2}%',
                formatter: function (params) {
                    var result = '';
                    params.forEach(function (item) {
                        result += '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:' + item.color + '"></span>' + item.seriesName + '&nbsp' + item.value + '%' + '<br />';
                    });

                    return result;
                }
                /*formatter: function (params, ticket, callback) {
                    console.log(params);
                    var showHtm="";
                    for(var i=0;i<params.length;i++){
                        //x轴名称
                        var name = params[i][1];
                        //名称
                        var text = params[i][3];
                        //值
                        var value = params[i][2];
                        showHtm+= text+ '--' + name + ' 得分：' + value+'<br>'
                    }
                    return showHtm;
                }*/
            },
            calculable: true,
            xAxis: [
                {
                    type: 'category',
                    show: true,
                    axisLabel: {
                        show: true,
                        interval: 'auto',
                        // formatter: '{value}%'
                    },
                    // data: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', '智能1号线']
                    data: config.xData
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    axisLabel: {
                        show: true,
                        interval: 'auto',
                        formatter: '{value}%'
                    },
                    show: true
                }
            ],
            grid: {
                x: 47,
                y: 74,
                x2: 0,
                y2: 21,
                borderWidth: 1
            },
            series: [
                {
                    name: '计划达成率',
                    type: 'bar',
                    barGap: 0,
                    label: labelOption,
                    data: config.planAchin,
                    type: 'line',
                    smooth: false, // 当为true时，就是光滑的曲线（默认为true）；当为false，就是折线不是曲线的了
                    symbolSize: 7, // 拐点圆的大小
                },
                {
                    name: '工时效率',
                    type: 'bar',
                    label: labelOption,
                    data: config.workEff,
                    type: 'line',
                    smooth: false, // 当为true时，就是光滑的曲线（默认为true）；当为false，就是折线不是曲线的了
                    symbolSize: 7, // 拐点圆的大小
                },
                {
                    name: '直通率',
                    type: 'bar',
                    label: labelOption,
                    data: config.passRate,
                    type: 'line',
                    smooth: false, // 当为true时，就是光滑的曲线（默认为true）；当为false，就是折线不是曲线的了
                    symbolSize: 7, // 拐点圆的大小
                }
            ]
        };
        myCharts.setOption(option);
        return myCharts;
    },

    /** @MethodName: checkBrowser
     * @Description: 判断网页是不是手机浏览器打开
     * @Return: 返回true表示为pc端打开，返回false表示为手机端打开
     * @Author: Yvan
     * @Date: 2019/9/19/0019  11:25
     *
     */
    checkBrowser: function () {
        var userAgentInfo = navigator.userAgent;
        var Agents = new Array("Android", "iPhone", "SymbianOS", "Windows Phone", "iPad", "iPod");
        var flag = true;
        for (var v = 0; v < Agents.length; v++) {
            if (userAgentInfo.indexOf(Agents[v]) > 0) {
                flag = false;
                break;
            }
        }
        return flag;
    },

    /** @MethodName: littleUrl.encode and littleUrl.decode
     * @Description: 对cookie中文的加密和解码，解决ios不识别cookie的问题
     * @Return:
     * @Author: Yvan
     * @Date: 2019/9/19/0019  11:25
     *
     */
    littleUrl: {
        // public method for url encoding
        encode: function (string) {
            return escape(this._utf8_encode(string));
        }
        ,
        // public method for url decoding
        decode: function (string) {
            return this._utf8_decode(unescape(string));
        }
        ,
        // private method for UTF-8 encoding
        _utf8_encode: function (string) {
            string = string.replace(/\r\n/g, "\n");
            var utftext = "";
            for (var n = 0; n < string.length; n++) {
                var c = string.charCodeAt(n);
                if (c < 128) {
                    utftext += String.fromCharCode(c);
                } else if ((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                } else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }
            }
            return utftext;
        }
        ,
        // private method for UTF-8 decoding
        _utf8_decode: function (utftext) {
            var string = "";
            var i = 0;
            var c = c1 = c2 = 0;
            while (i < utftext.length) {
                c = utftext.charCodeAt(i);
                if (c < 128) {
                    string += String.fromCharCode(c);
                    i++;
                } else if ((c > 191) && (c < 224)) {
                    c2 = utftext.charCodeAt(i + 1);
                    string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                    i += 2;
                } else {
                    c2 = utftext.charCodeAt(i + 1);
                    c3 = utftext.charCodeAt(i + 2);
                    string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }
            }
            return string;
        }
    },

    /** @MethodName: getInfo
     * @Description: 获取浏览器的信息
     * @Return:
     * @Author: Yvan
     * @Date: 2019/9/19/0019  11:25
     *
     */
    getInfo: function () {

        var s = "";

        s += " 网页可见区域宽：" + document.body.clientWidth + "\n";

        s += " 网页可见区域高：" + document.body.clientHeight + "\n";

        s += " 网页可见区域宽：" + document.body.offsetWidth + " (包括边线和滚动条的宽)" + "\n";

        s += " 网页可见区域高：" + document.body.offsetHeight + " (包括边线的宽)" + "\n";

        s += " 网页正文全文宽：" + document.body.scrollWidth + "\n";

        s += " 网页正文全文高：" + document.body.scrollHeight + "\n";

        s += " 网页被卷去的高(ff)：" + document.body.scrollTop + "\n";

        s += " 网页被卷去的高(ie)：" + document.documentElement.scrollTop + "\n";

        s += " 网页被卷去的左：" + document.body.scrollLeft + "\n";

        s += " 网页正文部分上：" + window.screenTop + "\n";

        s += " 网页正文部分左：" + window.screenLeft + "\n";

        s += " 屏幕分辨率的高：" + window.screen.height + "\n";

        s += " 屏幕分辨率的宽：" + window.screen.width + "\n";

        s += " 屏幕可用工作区高度：" + window.screen.availHeight + "\n";

        s += " 屏幕可用工作区宽度：" + window.screen.availWidth + "\n";

        s += " 你的屏幕设置是 " + window.screen.colorDepth + " 位彩色" + "\n";

        s += " 你的屏幕设置 " + window.screen.deviceXDPI + " 像素/英寸" + "\n";

        alert(s);

    },


    /** @MethodName: getInfoBrowser
     * @Description: 获取浏览器的宽高及其他
     * @Return:
     * @Author: Yvan
     * @Date: 2019/9/25/0025  10:03
     *
     */
    getInfoBrowser: function (res) {
        var baseNote = null;
        switch (res) {
            case 'clientWidth':                                 // 网页可见区域宽
                baseNote = document.body.clientWidth;
                break;
            case 'clientHeight':                                // 网页可见区域高
                baseNote = document.body.clientHeight;
                break;
            case 'offsetWidth':                                 // 网页可见区域宽(包括边线和滚动条的宽)
                baseNote = document.body.offsetWidth;
                break;
            case 'offsetHeight':                                // 网页可见区域高(包括边线的宽)
                baseNote = document.body.offsetHeight;
                break;
            case 'scrollWidth':                                 // 网页正文全文宽
                baseNote = document.body.scrollWidth;
                break;
            case 'scrollHeight':                                // 网页正文全文高
                baseNote = document.body.scrollHeight;
                break;
            case 'scrollTop':                                   // 网页被卷去的高(ff)
                baseNote = document.body.scrollTop;
                break;
            case 'scrollTopIe':                                 // 网页被卷去的高(ie)
                baseNote = document.documentElement.scrollTop;
                break;
            case 'scrollLeft':                                  // 网页被卷去的左
                baseNote = document.body.scrollLeft;
                break;
            case 'screenTop':                                   // 网页正文部分上
                baseNote = window.screenTop;
                break;
            case 'screenLeft':                                  // 网页正文部分左
                baseNote = window.screenLeft;
                break;
            case 'height':                                      // 屏幕分辨率的高
                baseNote = window.screen.height;
                break;
            case 'width':                                       // 屏幕分辨率的宽
                baseNote = window.screen.width;
                break;
            case 'availHeight':                                 // 屏幕可用工作区高度
                baseNote = window.screen.availHeight;
                break;
            case 'availWidth':                                  // 屏幕可用工作区宽度
                baseNote = window.screen.availWidth;
                break;
            case 'colorDepth':
                baseNote = window.screen.colorDepth;            // 你的屏幕设置几位彩色
                break;
            case 'deviceXDPI':                                  // 你的屏幕设置几位像素/尺寸
                baseNote = window.screen.deviceXDPI;
                break;
        }
        ;
        return baseNote;
    }
}