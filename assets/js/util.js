﻿// 定义一个全局变量用于接收解码后的cookie
// window.global = {userIdentity: window.parent.storageCookie};
// const testIp = '172.20.248.220:8088';
const testIp = '172.20.212.195:8088';
const ip = 'http://' + testIp + '/dms';

window.util = {

    requestget: function (url, cb) {
        var token = localStorage.getItem('storageCookie');
        if (!url) return;
        if (url[0] != '/' && url[0] != '\\') url = "/" + url;
        $.ajax({
            url: ip + url,
            headers: {
                token: token,
                // user: global.userIdentity.user.id,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'get',
            success: function (msg) {
                if (cb && typeof cb == "object") {
                    if (cb.success) {
                        cb.success(msg);
                    }
                } else if (cb && typeof cb == 'function') {
                    cb(msg);
                }
            },
            error: function (msg) {
                if (cb && typeof cb == "object") {
                    if (cb.error) {
                        cb.error(msg);
                    }
                }
                /*else if (cb && typeof cb == 'function') {
                    alert("请求失败！")
                }*/
            }
        })
    },

    requestpost: function (url, data, cb) {
        var token = localStorage.getItem('storageCookie');
        if (!url) return;
        if (url[0] != '/' && url[0] != '\\') url = "/" + url;
        //console.log("开始请求！"+e.url);
        $.ajax({
            url: ip + url,
            headers: {
                token: token,
                // user: global.userIdentity.user.id,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'post',
            data: JSON.stringify(data) || {},
            dataType: 'Json',
            //contentType:'application/json',
            success: function (msg) {
                if (cb && typeof cb == "object") {
                    if (cb.success) {
                        cb.success(msg);
                    }
                } else if (cb && typeof cb == 'function') {
                    cb(msg);
                }
            },
            error: function (msg) {
                if (cb && typeof cb == "object") {
                    if (cb.error) {
                        cb.error(msg);
                    }
                }
                /*else if (cb && typeof cb == 'function') {
                    alert("请求失败！")
                }*/
            }
        })
    },

};






